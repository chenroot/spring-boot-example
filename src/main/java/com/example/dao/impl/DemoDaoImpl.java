package com.example.dao.impl;

import com.example.common.base.AbstractMaterDaoImpl;
import com.example.dao.DemoDao;
import org.springframework.stereotype.Repository;

@Repository("demoDao")
public class DemoDaoImpl extends AbstractMaterDaoImpl implements DemoDao{
}
