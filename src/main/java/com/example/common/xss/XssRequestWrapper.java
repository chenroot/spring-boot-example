package com.example.common.xss;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XssRequestWrapper extends HttpServletRequestWrapper {

    private static final Whitelist JSON_WHITE_LIST = Whitelist.basic();
    private static final Document.OutputSettings JSOUP_OUTPUT_SETTINGS = new Document.OutputSettings();

    static {
        JSON_WHITE_LIST.removeTags("a");
        JSOUP_OUTPUT_SETTINGS.prettyPrint(false);
    }

    public XssRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String[] getParameterValues(String name) {
        //获取所有参数值的集合
        String[] results = this.getParameterMap().get(name);
        if (results != null && results.length > 0) {
            int length = results.length;
            for (int i = 0; i < length; i++) {
                //过滤参数值
                results[i] = striptXss(results[i]);
            }
            return results;
        }
        return null;
    }

    @Override
    public String getParameter(String name) {
        String value = super.getParameter(name);
        return striptXss(value);
    }

    @Override
    public String getHeader(String name) {
        String value = super.getHeader(name);
        return striptXss(value);
    }

    private String striptXss(String value){
        if(StringUtils.isNotBlank(value)){
            value = Jsoup.clean(value, "", JSON_WHITE_LIST, JSOUP_OUTPUT_SETTINGS);
        }
        return value;
    }
}