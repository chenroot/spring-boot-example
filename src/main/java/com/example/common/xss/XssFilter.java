package com.example.common.xss;

import org.apache.commons.lang.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;

public class XssFilter implements Filter {

    private static final String[] URL_PATHS = {};

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException{
        String urlPath = ((HttpServletRequest)request).getServletPath();
        if(checkXss(urlPath)){
            chain.doFilter(new XssRequestWrapper((HttpServletRequest)request), response);
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }

    private Boolean checkXss(String urlPath){
        return !(StringUtils.isNotBlank(urlPath)) && Arrays.asList(URL_PATHS).contains(urlPath);
    }
}
