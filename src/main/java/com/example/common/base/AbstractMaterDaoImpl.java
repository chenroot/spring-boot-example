package com.example.common.base;

import org.apache.ibatis.session.SqlSession;

import javax.annotation.Resource;

public abstract class AbstractMaterDaoImpl {

    @Resource(name = "materSqlSessionTemplate")
    private SqlSession readSessionTemplate;
    @Resource(name = "materSqlSessionTemplate")
    private SqlSession writeSessionTemplate;

    public SqlSession getReadSessionTemplate() {
        return readSessionTemplate;
    }

    public void setReadSessionTemplate(SqlSession readSessionTemplate) {
        this.readSessionTemplate = readSessionTemplate;
    }

    public void setWriteSessionTemplate(SqlSession writeSessionTemplate) {
        this.writeSessionTemplate = writeSessionTemplate;
    }

    public SqlSession getWriteSessionTemplate() {
        return writeSessionTemplate;
    }
}
