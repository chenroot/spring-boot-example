package com.example.controller;

import com.example.common.base.BaseController;
import com.example.common.config.DynamicConfig;
import com.example.common.util.RedisUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;

@Controller
@RequestMapping("/index")
@Slf4j
public class IndexController extends BaseController{

    @Autowired
    private DynamicConfig dynamicConfig;

    @Autowired
    private RedisUtil redisUtil;

    @RequestMapping("/index")
    @ResponseBody
    public String index(){
        log.info("info日志");
        log.warn("warn日志");
        log.error("error日志");
        redisUtil.set("environmentName", dynamicConfig.getEnvironmentName());
        return  "当前环境：" + redisUtil.get("environmentName");
    }

    @RequestMapping("/out")
    @ResponseBody
    public void out(){
        try(ServletOutputStream out = response.getOutputStream()){
            // 设置编码
            response.setHeader("content-type", "text/html;charset=utf-8");
            // 输出内容
            out.write(dynamicConfig.getEnvironmentName().getBytes("UTF-8"));
            out.flush();
        }catch (Exception e){

        }
    }
}
